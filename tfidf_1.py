from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import pandas as pd


# =============================================================================
# 
# =============================================================================

book_list = {}
book_table = []
with open(r"asv\book_names.txt") as b:
    for bookline in b:
        bookline = bookline.replace('\n','')
        book_list.update({ bookline.split('\t')[0]:bookline.split('\t')[1] })
        book_table.append({ 'order':int(bookline.split('\t')[0][:2]), 'book':bookline.split('\t')[1], 'code':bookline.split('\t')[0] })
book_table = pd.DataFrame(book_table)


line_list=[]
with open(r"asv\asv_utf8.txt") as f:
    for line in f: 
        try:
            line = line.replace('\n','')
            line_list.append({ 'code':line.split('\t')[0], 'chapter':line.split('\t')[1], 
                              'verse':line.split('\t')[2], 'text':line.split('\t')[5] })
        except: continue

df = pd.DataFrame(line_list)
df = pd.merge(df, book_table, left_on='code', right_on='code', how='left')
df['length'] = df['text'].apply(lambda x: len(x.split()))



#%%

corpus = [
    'This is the first document.',
    'This document is the second document.',
    'And this is the third one.',
    'Is this the first document?',
]
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(corpus)

print(vectorizer.get_feature_names())
print(X)

ff = pd.DataFrame(X.todense(), columns=vectorizer.get_feature_names())
