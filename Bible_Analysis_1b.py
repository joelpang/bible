

import re, operator

import pandas as pd
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import re, operator
from collections import Counter 

#myfile = open("asv\\asv_utf8.txt")  
#myfile = open("ylt\\ylt_utf8.txt")
myfile = open("NIV")

s = myfile.read()
myfile.close()


s = s.lower()
wordlist = re.split(r'\W',s)

stop_words = set(stopwords.words("english"))
stop_words.update(['and','unto','shall','thou','thy','ye','thee','upon','hath','shalt'])


#stop_words={} # enable for full set




freqlist={}

for wd in wordlist:
    if wd not in stop_words and not wd.isdigit():
        if wd in freqlist:
            freqlist[wd] = freqlist[wd]+1
        else:
            freqlist[wd] = 1

freqlistsort = sorted(freqlist.items(), key=operator.itemgetter(1) ,reverse=True)
for i in range(100):
    print(freqlistsort[i])


freqlistsort = pd.DataFrame(freqlistsort)
    
