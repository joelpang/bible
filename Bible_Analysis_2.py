

import re, operator
import pandas as pd
import json
from collections import Counter 
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords

import numpy as np
import matplotlib.pyplot as plt


book_list = {}
book_table = []
with open(r"asv\book_names.txt") as b:
    for bookline in b:
        bookline = bookline.replace('\n','')
        book_list.update({ bookline.split('\t')[0]:bookline.split('\t')[1] })
        book_table.append({ 'order':int(bookline.split('\t')[0][:2]), 'book':bookline.split('\t')[1], 'code':bookline.split('\t')[0] })
book_table = pd.DataFrame(book_table)


line_list=[]
with open(r"asv\asv_utf8.txt") as f:
    for line in f: 
        try:
            line = line.replace('\n','')
            line_list.append({ 'code':line.split('\t')[0], 'chapter':line.split('\t')[1], 
                              'verse':line.split('\t')[2], 'text':line.split('\t')[5] })
        except: continue

df = pd.DataFrame(line_list)
#df['book'] = df['book'].map(book_list)
df = pd.merge(df, book_table, left_on='code', right_on='code', how='left')
df['length'] = df['text'].apply(lambda x: len(x.split()))


#%%

x = df.groupby('order').sum()['length']
y = df.groupby('book').sum()['length']

plt.rcParams["figure.dpi"] = 70

fig, ax = plt.subplots(figsize=(10,6), tight_layout=True)
plt.bar(x.index, x.values, width=0.5)

plt.grid(linestyle=':')
plt.axvline(0, color="k", linewidth=0.5, linestyle='-')


#%%

import string

#stop_words = set(stopwords.words("english"))
#stop_words.update(['and','unto','shall','thou','thy','ye','thee','upon','hath','shalt'])
#stop_words = ['and','unto','shall','thou','thy','ye','thee','upon','hath','shalt','the','of','and','he','she','i','his','her']

v = df[df['book']=='Genesis']['text'].values

def most_frequent (input_words):
    freqlist={}
    words = []
    
    for w in input_words: 
        for y in w.split(): words.append(y)
        
    words = [w.translate(str.maketrans('', '', string.punctuation)) for w in words]
    words = ' '.join(words)

    for wd in words.split():
        wd = wd.lower()
#        if wd not in stop_words and not wd.isdigit():
        if not wd.isdigit():
            if wd in freqlist:
                freqlist[wd] = freqlist[wd]+1
            else:
                freqlist[wd] = 1
    
    freqlistsort = sorted(freqlist.items(), key=operator.itemgetter(1) ,reverse=True)
    return freqlistsort[:50]


print( most_frequent(v) )


#%%

# Execute function across all books

wf = book_table.copy()
wf['freq'] = np.nan
wf.set_index('book',inplace=True)

for i in wf.index:
    wf.loc[i,'freq'] = [ most_frequent(df[df['book']==i]['text'].values) ]


#%%
    

# Put entire bible into a 66 member list
    
corpus = []
    
    
from sklearn.feature_extraction.text import TfidfVectorizer


vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(corpus)

print(vectorizer.get_feature_names())
print(X)

ff = pd.DataFrame(X.todense(), columns=vectorizer.get_feature_names())