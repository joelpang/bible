from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import pandas as pd
import string
import matplotlib.pyplot as plt


# =============================================================================
# TF-IDF 
# =============================================================================


# Put bible into dataframe

def get_bible():

    book_list = {}
    book_table = []
    with open(r"asv\book_names.txt") as b:
        for bookline in b:
            bookline = bookline.replace('\n','')
            book_list.update({ bookline.split('\t')[0]:bookline.split('\t')[1] })
            book_table.append({ 'order':int(bookline.split('\t')[0][:2]), 'book':bookline.split('\t')[1], 'code':bookline.split('\t')[0] })
    book_table = pd.DataFrame(book_table)
    
    
    line_list=[]
    with open(r"asv\asv_utf8.txt") as f:
        for line in f: 
            try:
                line = line.replace('\n','')
                line_list.append({ 'code':line.split('\t')[0], 'chapter':line.split('\t')[1], 
                                  'verse':line.split('\t')[2], 'text':line.split('\t')[5] })
            except: continue
    
    df = pd.DataFrame(line_list)
    df = pd.merge(df, book_table, left_on='code', right_on='code', how='left')
    df['length'] = df['text'].apply(lambda x: len(x.split()))
    df.dropna(subset=['book'], inplace=True)
    
    book_table = book_table[book_table.order<=66]
    
    return df,book_table


df,book_table = get_bible()

#%%

corpus = []
for b in book_table['book']:
    y = [x.translate(str.maketrans('', '', string.punctuation)) for x in df[df.book==b]['text'].values]
    y = [x+' ' for x in y]
    y = pd.DataFrame(y)
    y = y.values.sum()
    corpus.append( y )


stopwords = [
        'the', 'and', 'of', 'that', 'to', 'in', 'was', 'they', 'he', 'his', 'is', 'with',
        'unto', 'shall', 'for', 'them', 'him', 'as', 'their', 'her', 'are', 'were', 'it',
        'we', 'ye', 'you', 'be', 'thy', 'thee', 'me', 'my', 'thou', 'but', 'not', 'your'
        ]


#%%
    
# =============================================================================
# First method TF IDF score
# =============================================================================



vectorizer = TfidfVectorizer(stop_words=stopwords)
X = vectorizer.fit_transform(corpus)

ff = pd.DataFrame(X.T.todense(), columns=book_table['book'], index=vectorizer.get_feature_names())


freq = {}
for c in ff.columns:
    freq[str(c)] = list(ff.loc[:,c].sort_values(ascending=False).index[:30]) 



#%%

# =============================================================================
# Second method - count and percentage
# =============================================================================


# Total count of this word in book <- af
    
vectorizer = CountVectorizer()
X = vectorizer.fit_transform(corpus) 
af = pd.DataFrame(X.T.todense(), columns=book_table['book'], index=vectorizer.get_feature_names())


# Words in book / total words across books
# 1 = purely in this book, high = concentrated in this book
sf = af.copy().apply(lambda x: x/np.sum(x), axis=1)

# Words in book / total words in book 
# 1 = purely in this book, high = concentrated in this book
tf = af.copy().apply(lambda x: x/np.sum(x), axis=0)

# Total percentage weight over entire text
# Accounts for both dimensions, but not easy to compare across
pf = 100*af.copy() / np.sum(af.values)





#%%

plt.rcParams["figure.dpi"] = 70

def word_percent(df, word, t):
    x = df.loc[word,:]
    fig, ax = plt.subplots(figsize=(14,7), tight_layout=True)
    plt.bar(x.index, x.values, width=0.5)
    plt.grid(linestyle=':')
    plt.axhline(x.mean(), color="b", linewidth=0.5, linestyle='--')
    plt.xticks(rotation='vertical')
    plt.margins(0.01)
    if t==0:
        plt.title('Count of \''+word+'\' across books') 
    elif t==1:
        plt.title('Distribution of \''+word+'\'') # redundant
    elif t==2:
        plt.title('TF-IDF score of \''+word+'\'')
    elif t==3:
        plt.title('Term frequency of \''+word+'\'')

    plt.show()


#word_percent(sf, 'love', 1)

word_percent(af, 'love', 0)

word_percent(tf, 'love', 3)

word_percent(ff, 'love', 2)















